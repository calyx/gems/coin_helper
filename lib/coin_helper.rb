require_relative 'coin_helper/configuration'
require_relative 'coin_helper/data'

module CoinHelper
  def self.configuration
    @configuration ||= Configuration.new
  end
  def self.configure
    yield(configuration)
  end
end

require_relative "coin_helper/engine"