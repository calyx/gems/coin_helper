module CoinHelper
  if defined?(::Rails::Engine)
    class Engine < ::Rails::Engine
      isolate_namespace CoinHelper

      config.to_prepare do
        if CoinHelper.configuration.auto_include
          ApplicationController.helper(CoinHelper::CryptocurrencyHelper)
        end
      end

      initializer "coin_helper.assets.precompile" do |app|
        app.config.assets.precompile += %w(
          coin_helper/color/22/*.png
        )
      end
    end
  end
end
